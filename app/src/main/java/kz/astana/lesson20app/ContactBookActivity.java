package kz.astana.lesson20app;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class ContactBookActivity extends AppCompatActivity {

    private final int REQUEST_CODE_PERMISSION_READ = 100;
    private final int REQUEST_CODE_PERMISSION_WRITE = 200;
    private final int REQUEST_CODE_PERMISSION_UPDATE = 300;

    private ListView listView;
    private TextInputEditText nameEditText, phoneEditText;
    private TextInputLayout nameLayout, phoneLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_book);

        listView = findViewById(R.id.listView);
        nameEditText = findViewById(R.id.nameEditText);
        phoneEditText = findViewById(R.id.phoneEditText);
        nameLayout = findViewById(R.id.nameLayout);
        phoneLayout = findViewById(R.id.phoneLayout);

        Button loadButton = findViewById(R.id.loadButton);
        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        ContactBookActivity.this,
                        new String[]{
                                Manifest.permission.READ_CONTACTS,
                                Manifest.permission.CALL_PHONE
                        },
                        REQUEST_CODE_PERMISSION_READ
                );
            }
        });

        Button insertButton = findViewById(R.id.insertButton);
        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        ContactBookActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        REQUEST_CODE_PERMISSION_WRITE
                );
            }
        });

        Button updateButton = findViewById(R.id.updateButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        ContactBookActivity.this,
                        new String[]{Manifest.permission.WRITE_CONTACTS},
                        REQUEST_CODE_PERMISSION_UPDATE
                );
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION_READ &&
                permissions[0].equals(Manifest.permission.READ_CONTACTS) &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // TODO LOAD CONTACTS LIST
            Cursor cursor = getContentResolver()
                    .query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

            if (cursor.moveToFirst()) {
                List<String> contacts = getContacts(cursor);

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        ContactBookActivity.this,
                        android.R.layout.simple_list_item_1,
                        contacts
                );

                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // TODO CALL PHONE
                        String contact = adapter.getItem(position);
                        String[] s = contact.split(":");
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel: " + s[1]));
                        startActivity(intent);
                    }
                });
            }
        } else if (requestCode == REQUEST_CODE_PERMISSION_WRITE &&
                permissions[0].equals(Manifest.permission.READ_CONTACTS) &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // TODO CREATE CONTACT
            String name = nameEditText.getText().toString();
            String phone = phoneEditText.getText().toString();

            if (TextUtils.isEmpty(name) || TextUtils.isEmpty(phone)) {
                if (TextUtils.isEmpty(name)) {
                    nameLayout.setError("Enter name");
                }
                if (TextUtils.isEmpty(phone)) {
                    phoneLayout.setError("Enter phone");
                }
            } else {
                nameLayout.setError(null);
                phoneLayout.setError(null);

                Intent intent = new Intent();
                intent.setAction(ContactsContract.Intents.Insert.ACTION);
                intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, phone);
                intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
                startActivity(intent);
            }
        } else if (requestCode == REQUEST_CODE_PERMISSION_UPDATE &&
                permissions[0].equals(Manifest.permission.WRITE_CONTACTS) &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // TODO UPDATE CONTACT
            Cursor cursor = getContentResolver()
                    .query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cursor.moveToFirst()) {
                int lookupKeyIndex = cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY);
                String lookupKey = cursor.getString(lookupKeyIndex);

                int idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
                long id = cursor.getLong(idIndex);

                Uri contactUri = ContactsContract.Contacts.getLookupUri(id, lookupKey);

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_EDIT);
                intent.setDataAndType(contactUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                startActivity(intent);

                cursor.close();
            }
        }
    }

    private List<String> getContacts(Cursor cursor) {
        ArrayList<String> contacts = new ArrayList<>();

        do {
            int idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
            String id = cursor.getString(idIndex);

            int nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            String name = cursor.getString(nameIndex);

            int hasPhoneNumberIndex = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
            String hasPhoneNumber = cursor.getString(hasPhoneNumberIndex);

            if (Integer.parseInt(hasPhoneNumber) > 0) {
                Cursor phoneCursor = getContentResolver().query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        new String[]{id},
                        null
                );
                if (phoneCursor.moveToFirst()) {
                    String phoneNumber = getPhoneNumbers(phoneCursor);
                    contacts.add(name + ":" + phoneNumber);
                }
            }
        } while (cursor.moveToNext());

        return contacts;
    }

    private String getPhoneNumbers(Cursor phoneCursor) {
        StringBuilder phoneNumber = new StringBuilder();
        do {
            int phoneNumberIndex = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            phoneNumber.append(phoneCursor.getString(phoneNumberIndex)).append("\n");
        } while (phoneCursor.moveToNext());
        phoneCursor.close();

        return phoneNumber.toString();
    }
}